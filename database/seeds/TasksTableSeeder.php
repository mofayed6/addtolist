<?php

use Illuminate\Database\Seeder;

use Faker\Generator as Faker;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Task', 10)->create();
    }

}
