<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Model;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

//        factory('App\Group', 2)->create();

            \App\Group::create([
                'name' => 'Group A',
            ]);

            \App\Group::create([
                'name' => 'Group B',
            ]);



    }
}
