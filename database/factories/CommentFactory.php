<?php

use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    return [
        'comment' => $faker->text(),
        'task_id' => factory('App\Task')->create()->id,
    ];
});
