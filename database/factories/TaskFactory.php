<?php

use Faker\Generator as Faker;

$factory->define(\App\Task::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence(5),
        'description' => $faker->text(),
        'date' => $faker->date(),
        'group_id' => $faker->randomElement([1,2]),
        'completed' => $faker->randomElement(['Done', 'Delayed','Pending']),

    ];
});
