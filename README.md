
1.  Clone this repository and cd into it
2.  Run `composer install` and `npm install` 
3.  Set up pusher account
4.  Set up your database in your `.env`
6.  Execute `npm run dev` to build project 
7.  Execute `php artisan serve` to run the project in browser

