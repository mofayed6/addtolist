<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

	protected $fillable = ['title','completed','group_id','date','description','sort'];

    public function groups(){
        return $this->belongsTo(Group::class);
    }
}
