<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CreateComment;
use App\Events\EditSort;
use App\Events\EditTitle;
use App\Events\EditType;
use App\Group;
use Illuminate\Http\Request;
use App\Events\TaskCreated;
use App\Events\TaskRemoved;
use App\Task;
use DB;

class TaskController extends Controller
{

    /**
     * this group function get all groups with related tasks
     * @return array
     */
    public function groups()
    {
        $groups = Group::with('tasks')->get();
        return $groups;
    }

    /**
     * store new task
     * support broadcast real time
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
      // dd($request->all());
        $task = Task::create($request->all());

        broadcast(new TaskCreated($task));

        return response()->json($task, 200);
    }

    /**
     * update task when send date update it and update status task
     * support broadcast real time
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        $task = Task::find($id);
        $task->update($request->all());

        broadcast(new EditType($task));
     //   broadcast(new EditTitle($task));

        return response()->json("updated");
    }

    /**
     * delete task
     * support broadcast real time
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $task = Task::find($id);
        broadcast(new TaskRemoved($task));
        Task::destroy($id);

        return response()->json("deleted");
    }

    /**
     * get comment related task
     * get task when send id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment($id)
    {
        $comments = Comment::where('task_id', $id)->orderBy('created_at', 'DESC')->get();
        $task = Task::find($id);

        return response()->json(['comments' => $comments, 'task' => $task], 200);

    }

    /**
     * save comment in table comments
     * support broadcast real time
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeComment(Request $request)
    {
        $comment = Comment::create($request->all());
        broadcast(new CreateComment($comment));

        return response()->json($comment, 200);
    }

    /**
     * function update sort task
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSort(Request $request)
    {
        //dd($request->all());
        $tasks = $request->data['tasks'];
        foreach ($tasks as $key=>$value){
            foreach ($value as  $k=>$v){
                 $data = Task::find($v['id'])->update(['sort'=>$k,'group_id'=>$key]);
                 $alldata =Task::find($v['id']);

            }
        }


        broadcast(new EditSort($alldata));

        return response()->json('updated', 200);

    }


}
