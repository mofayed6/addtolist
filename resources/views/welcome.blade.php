<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Realtime todo app</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="https://unpkg.com/vuejs-datepicker"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    {{--  Style  --}}
        {{--<link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">--}}

        <!-- Styles -->
        <style>

            body{
                background-color:#f5f5f5
            }
            .header{
                background-color: #ffffff;
                padding: 8px 0 8px 0 ;
                border-radius: 0 0 7px 7px;
                border: #e1e1e1 solid 1px;
            }

            .header img{
                margin-left: 13px;
            }

            .content .cont{
                background-color: #ffffff;
                margin: 5px;
                border-radius: 8px;
                border: #e1e1e1 solid 1px;
                height: 100%;
            }

            .cont .titel{
                margin: 8px;
            }

            .footer{
                background-color: #ffffff;
                padding: 8px 0 8px 0 ;
                border-radius:7px 7px 0 0;
                border: #e1e1e1 solid 1px;
                text-align: center;
            }
            .footer p{
                margin-top: 30px;
            }

            .new-todo,
            .edit {
                position: relative;
                margin: 0;
                width: 100%;
                font-size: 24px;
                font-family: inherit;
                font-weight: inherit;
                line-height: 1.4em;
                border: 0;
                color: inherit;
                padding: 6px;
                border: 1px solid #999;
                -webkit-box-shadow: inset 0 -1px 5px 0 rgba(0, 0, 0, 0.2);
                box-shadow: inset 0 -1px 5px 0 rgba(0, 0, 0, 0.2);
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .new-todo {
                padding: 16px 16px 16px 60px;
                border: none;
                background: rgba(0, 0, 0, 0.003);
                -webkit-box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
                box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="header">
            <img src="{{ asset('img/logo.jpg') }}">
        </div>
        <br>

          <div id="app">
                    <todo-app></todo-app>

          </div>

    </div>

    <script src="https://unpkg.com/vuejs-datepicker"></script>

          <script src="{{mix('js/app.js')}}"></script>
    </body>
</html>
