let actions = {
    /**
     * function get all groups with tasked related
     * @param commit
     * @constructor
     */
    GET_GROUPS({
        commit
    }) {
        axios.get('/api/groups')
            .then(res => {
                {
                    //console.log(res.data)
                    commit('GET_GROUPS', res.data)
                }
            })
            .catch(err => {
                console.log(err)
            })
    } ,
}

export default actions