<?php

use Illuminate\Http\Request;


/*
--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::get('groups','TaskController@groups');
Route::get('comments/{id}','TaskController@comment');
Route::post('todos','TaskController@store');
Route::post('todos-sort','TaskController@updateSort');
Route::post('todos/{id}','TaskController@update');
Route::delete('todos/{id}','TaskController@delete');
Route::post('comment','TaskController@storeComment');
